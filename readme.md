# Markbook

NodeJS server that renders markdown files to HTML and serves them

## Usage

### Container

Start using docker-compose

```bash
docker-compose up --build
```

Build container manually

```bash
make container
make run
```

### Local Host

```bash
make deploy
make start
```

### Development mode

```bash
make install
make
```
